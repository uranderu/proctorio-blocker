# proctorio-blocker
Block the spyware that is called proctorio with this list for your Pi-Hole!

# How to install in Pi-Hole
Navigate to http://pi.hole/admin/groups-adlists.php
and under Add a new adlist copy paste:
https://raw.githubusercontent.com/uranderu/proctorio-blocker/master/blocklist.txt

# Download
Download Raw
https://raw.githubusercontent.com/uranderu/proctorio-blocker/master/blocklist.txt
Download from Releases
https://github.com/uranderu/proctorio-blocker/releases

# Credits:
I read this blogpost from Soatok: [link](https://soatok.blog/2020/09/12/edutech-spyware-is-still-spyware-proctorio-edition/) about proctorio and decided I wanted to turn the domains he listed into an easy to import pihole adlist. I used this repo as a template: [link](https://github.com/qfoxb/redshell-blocker)

I highly recommend you check out both the article and repo!
